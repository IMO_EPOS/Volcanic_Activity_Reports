# -*- coding: utf-8 -*-
"""
Populate the reference tables of the VolActRep DB for the first time:
'volcano' from a csv file for convenience,
'agency'  fixed here (just IMO)
'contact' ... maybe csv file
'country' fixed here (just Iceland)

call as:

python write_first_data_to_db.py database/volcano_table_nocomm.csv -v

2017-11-21 tim: created
2017-11-22 tim: made 'safe_add' an external function
2017-11-28 tim: added 'alt_name' as new Volcano keyword
"""

# standard library imports:
import csv
import argparse
import re

# SqlAlchemy imports
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# Use the local config parser and datamodel
from parse_config import parse_from_cfg
from volc_act_rep_datamodel import (
    Agency, Volcano, Country, 
    #get_the_base, 
    #Contact, Report, VolcanoReports, 
)
from my_db_utils import safe_add # my function to INSERT or skip if CONFLICT


# This is the main function
def parse_csv_volcanos():
    """Take a csv file, parse it and (hopefully) return its contents as obj.
    """
    # check input
    parser = argparse.ArgumentParser(
        description='Parse Volc.Act.Report csv file')
    parser.add_argument('file', type=str)
    parser.add_argument('-v', '--verbose', help='increase output verbosity', 
                        action='store_true')
    args = parser.parse_args()
    
    # Defining verbose print depending on requested verbosity
    if args.verbose:
        def vprint(*args):
            # print all args in one line
            for arg in args:
                print arg, 
            print
    else:
        vprint = lambda *a: None  # do-nothing function
    
    # get db connection info from config file
    db_url, db_info = parse_from_cfg()
    # setup db connection and session
    engine = create_engine(db_url)
    session = sessionmaker()
    session.configure(bind=engine)
    s = session()
    
    # COUNTRY
    vprint('Iceland')
    eid, msg, id_country = safe_add(
        s, Country(name='Iceland', iso_code='IS'))
    
    # AGENCY
    vprint('IMO')
    agency1 = Agency(name='Icelandic Meteorological Office',
        abbreviation='IMO', 
        address='Bústaðavegur 7-9, 104, Reykjavík, ICELAND', 
        www='www.vedur.is', 
        id_country=id_country)
    eid, msg, id_agency = safe_add(s, agency1)
    
    # CONTACT
    # need contact info, but maybe later...
    # Or simply put in reports and try to add contact name
    #   and catch if already exists.
    
    
    # Read csv, assign contents to volcano table
    with open(args.file, 'rb') as f:
        reader = csv.DictReader(f, delimiter=';', quoting=csv.QUOTE_NONE)
        for r in reader:
            vprint(r['name'])
            if not re.match('^[0-9]+$', r['icao_code']):
                r['icao_code']=None
                
            # get all volcano info
            v = Volcano(name=r['name'], si_name=r['si_name'],
                alt_name=r['alt_name'], 
                lat_dd=r['lat_dd'], lon_dd=r['lon_dd'], 
                lat_dms=r['lat_dms'], lon_dms=r['lon_dms'], 
                lat_nggmm=r['lat_nggmm'], lon_ngggmm=r['lon_ngggmm'], 
                height=r['height'], icao_code=r['icao_code'], 
                area=r['area'], id_country=id_country)
            #vprint(r)
            #s.query(Volcano).filter(Volcano.name == r['name']).all()
            eid, msg, vid = safe_add(s, v)
            if eid == 'unique':
                # if it's non-unique because of smithsonian id number,
                # modify the id number and retry:
                if 'icao_code' in msg:
                    v.icao_code = (v.icao_code + '001')
                    print ('Warning: Have new volcano with same icao id '
                           'as existing one. Modifying new icao id...')
                    safe_add(s, v)
                    # this is not great, but allows including an extra entry
                    # that for whatever reason has the same id as another
                    # volcano (this can be reviewed later in the DB).
            
            
        
    


# def main() over.
if __name__ == '__main__':
    parse_csv_volcanos()
