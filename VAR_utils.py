# -*- coding: utf-8 -*-
"""
This script should contain function definitions for all available database
methods usable in the Volcanic Activity Reports project.

2017-11-24 tim
"""


# standard library imports:
import re
from datetime import datetime

# SQLAlchemy inports:
from sqlalchemy import or_
from sqlalchemy.orm.exc import MultipleResultsFound

# local app imports:
from my_db_utils import safe_add # my function to INSERT or skip if CONFLICT
from volc_act_rep_datamodel import (
    Agency, Contact, Country, Report, Volcano, VolcanoReport,
) # my database classes
from utils import ISO3166Utils


def insert_contact(session, d_in):
    """Require dict input or name string, put new contact into DB.
    """
    id_contact = None
    name = None
    keys = ['title', 'email', 'phone', 'gsm', 'comment', 'role']
    try: # assuming dict given:
        container = {'name':d_in['name']} # throw TypeError if not dict
        name = container['name']
        key = 'id_agency'
        if key in d_in and d_in[key]:
            id_x = d_in[key]
        elif 'agency' in d_in and d_in['agency']:
            id_x = get_agency_id(session, d_in['agency'])
            if not id_x:
                id_x = insert_agency(session, d_in['agency'])
        else:
            id_x = None
        container[key] = id_x
        for key in keys:
            if key in d_in:
                container[key] = d_in[key]
        eid, msg, id_contact = safe_add(session, Contact(**container))
    except TypeError: # assuming just string given:
        if d_in:
            name = d_in
            eid, msg, id_contact = safe_add(
                session, Contact(name=d_in))
    if eid == 'unique':
        id_contact = get_contact_id(session, name)
    return id_contact


def get_country_id(session, name):
    if not name:
        return None
    try:
        id_x = session.query(Country.id).filter(
            or_(Country.name == name, 
                Country.iso_code == name)).scalar()
    except MultipleResultsFound as e:
        print ('ERROR: Given country "{}" in current query found ',
            'multiple times in database!').format(name)
        print '       Database needs to be fixed!'
        raise e
    return id_x


def get_agency_id(session, name):
    if not name:
        return None
    try:
        id_x = session.query(Agency.id).filter(
            or_(Agency.name == name, 
                Agency.abbreviation == name)).scalar()
    except MultipleResultsFound as e:
        print ('ERROR: Given agency "{}" in current query found ',
            'multiple times in database!').format(name)
        print '       Database needs to be fixed!'
        raise e
    return id_x


def get_contact_id(session, name):
    if not name:
        return None
    try:
        id_x = session.query(Contact.id).filter(
            Contact.name == name).scalar()
    except MultipleResultsFound as e:
        print ('ERROR: Given contact "{}" in current query found ',
            'multiple times in database!').format(name)
        print '       Database needs to be fixed!'
        raise e
    return id_x


def insert_agency(session, d_agency):
    id_agency = None
    keys = ['abbreviation', 'address', 'www', 'infos']
    try: # assuming dict given:
        agency = {'name':d_agency['name']} # throw TypeError if not dict
        if 'id_country' in d_agency and d_agency['id_country']:
            id_country = d_agency['id_country']
        elif 'country' in d_agency and d_agency['country']:
            id_country = get_country_id(session, d_agency['country'])
        else:
            id_country = None
        agency['id_country'] = id_country
        for key in keys:
            if key in d_agency:
                agency[key] = d_agency[key]
        eid, msg, id_agency = safe_add(session, Agency(**agency))
    except TypeError: # assuming just string given:
        if d_agency:
            eid, msg, id_agency = safe_add(session, Agency(name=d_agency))
    return id_agency


def insert_country(session, country, country_code=None):
    if country_code: # have code
        if not country: # but not name
            country = ISO3166Utils.short2full(country_code)
            if not country:
                print 'Error: Given country code does not exist: {}'.format(
                    country_code)
                print '       (Cannot insert anything here.)'
                return ('input_error','unknown country', None, None)
        eid, msg, xid = safe_add(
            session, Country(name=country, iso_code=country_code))
    elif len(country)==2: # actually code given instead of name
        country_code = country
        country = ISO3166Utils.short2full(country_code)
        eid, msg, xid = safe_add(
            session, Country(name=country, iso_code=country_code))
    elif country: # have name, but no code given
        country_code = ISO3166Utils.full2short(country) # allows 1 wrong char
        if not country_code:
            print 'Error: Given country does not exist: {}'.format(
                country_code)
            print '       (Cannot insert anything here.)'
            return ('input_error','unknown country', None, None)
        eid, msg, xid = safe_add(
            session, Country(name=country, iso_code=country_code))
    else:
        print 'Error: Have received empty country info to insert!'
        print '       (Cannot insert anything here.)'
        eid, msg, xid = ('input_error','empty input given',None)
    return eid, msg, xid, country


def insert_report(session, report_dict):
    klist = ['date_publication', 'date_valid_from', 'date_valid_to']
    for k in klist:
        report_dict[k] = check_date(report_dict[k])
    eid, msg, xid = safe_add(session, Report(**report_dict))
    return eid, msg, xid


def request_report_by_pdf_path(session, pdf):
    xid = session.query(Report.id).filter(Report.file_pdf == pdf).scalar()
    return xid

def insert_volcano_report(session, volc_dict):
    # have to find corresponding volcano id:
    id_volcano = session.query(Volcano.id).filter(
        or_(Volcano.name == volc_dict['id_volcano'],
            Volcano.si_name == volc_dict['id_volcano'],
            Volcano.alt_name == volc_dict['id_volcano'])).scalar()
    volc_dict['id_volcano'] = id_volcano
    eid, msg, xid = safe_add(session, VolcanoReport(**volc_dict))
    return eid, msg, id_volcano


def check_date(datestr):
    """
    Check and if necessary convert given date string to ISO format    
    """
    if re.match(r'\d{1,2}\.\d{1,2}\.\d{4}', datestr):
        d = datetime.strptime(datestr,'%d.%m.%Y')
        datestr = d.strftime('%Y-%m-%d')
    elif re.match(r'\d{1,2}\.\w{3,8}\.\d{4}', datestr):
        d = datetime.strptime(datestr,'%d.%B.%Y')
        datestr = d.strftime('%Y-%m-%d')
    return datestr

def is_valid_date(datestr):
    """
    Return true or false if datestr is recognized date format or not.
    """
    if re.match(r'\d{1,2}\.\d{1,2}\.\d{4}', datestr):
        return True
    elif re.match(r'\d{1,2}\.\w{3,8}\.\d{4}', datestr):
        return True
    elif re.match(r'\d{4}-\w{2}-\d{2}', datestr):
        return True
    return False
    








