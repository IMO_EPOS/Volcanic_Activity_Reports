# -*- coding: utf-8 -*-
"""
Main script to start and run a Flask web server.

It's configuration details must be given in a config file.
The methods for specific web service requests are contained in queries.py
Part of the code is copied from Fjalar's fwss web_server. 

Created on Wed Nov 29 12:17:27 2017
@author: tim
"""


__version__ = '0.1'
__author__  = 'Tim Sonnemann'

# standard library imports:
import sys # for exit on error and terminal output
import os.path # to check whether file exists
from httplib import (
    NO_CONTENT,
    OK,
    NOT_FOUND,
    CONFLICT,
    BAD_REQUEST,
    INTERNAL_SERVER_ERROR,
    CREATED,
)

# third party imports:
from flask import (
    Flask,
    request,
    render_template,
    Response,
) # Flask server app, info request class
from flask_cors import CORS # to enable CORS on all responses

# local app imports:
from parse_config import parse_from_cfg # my config parser
from volc_act_rep_queries import Queries

###

app = Flask(__name__) # instantiate Web Server Gateway Interface application
CORS(app) # enable CORS for all responses

## Instantiate a database connection
db_url = parse_from_cfg("database") # returning tuple
queries = Queries(db_url[0])

## general tools
def preset_result():
    status_code = NO_CONTENT
    result = (
        Response((
            '<h1>{} - This service gave no results.</h1>'
            'Either the database is empty '
            'or necessary tables are missing data.').format(
                status_code)),
        status_code)
    return status_code, result


###
### Services begin ###
###

@app.route('/', alias=True) # set service endpoint(s) for current function
@app.route('/volcanoactivity/') # redirect from '_' typo
def hello():
    purpose = 'querying weekly volcanic activity reports'
    rooturl = request.url_root
    mailtocontact = 'tim@vedur.is'
    services = [
        {'endpoint':'volcano_activity/report',
         'description':'Request info about all available reports.'
        },
        {'endpoint':'volcano_activity/report/<report_id>',
         'description':'Request info about reports by ID'
        },
        {'endpoint':'volcano_activity/volcano',
         'description':'Request info about all available volcanoes.'
        },
        {'endpoint':'volcano_activity/country',
         'description':'GET or POST countries (maybe).'
        },
        {'endpoint':'volcano_activity/country/<name>',
         'description':'Request info about a country.'
        },
    ]
    return render_template('home_1.html', # should be in 'templates/'
                           purpose=purpose,
                           rooturl=rooturl,
                           services=services,
                           mailtocontact=mailtocontact)


@app.route('/volcano_activity/report', methods=['GET', 'POST'])
def report():
    """
    Query report files:
    Get list of all reports, or limit by date and/or agency, e.g.
    /volcano_activity/report?date_from=2017-01-01&date_to=2017-03-01&agency=IMO
    """
    #Preset variables
    status_code, result = preset_result()
    ## Handle GET requests
    if request.method == 'GET':
        # 1) Argument handling (mostly in queries.py)
        args = request.args
        # 2) Query database
        query_result, status_code = queries.report_list_get(args)
        # 3) Process the response 
        if status_code == OK:
            result = (Response(query_result), status_code)
            result[0].headers['Content-type'] = 'application/json'
    
    return result


@app.route('/volcano_activity/report/<id_report>', methods=['GET'])
def report_by_report_id(id_report):

    #Preset variables
    status_code, result = preset_result()
    args = {'id_report':id_report}
    query_result, status_code = queries.report_list_get(args)
    if status_code == OK:
        result = (Response(query_result), status_code)
        result[0].headers['Content-type'] = 'application/json'
    elif status_code == NO_CONTENT:
        result = NO_CONTENT
    return result


@app.route('/volcano_activity/volcano', methods=['GET', 'POST'])
def volcanoes():

    #Preset variables
    status_code, result = preset_result()

    ## Handle GET requests
    if request.method == 'GET':
    
        # 1) Argument handling
        args = {'date_from':None, 'date_to':None, 'agency':None}
        # TODO: Implement such key values...
    
        # 2) Query database
        query_result, status_code = queries.volcano_list_get(args)
    
        # 3) Process the response 
        if status_code == OK:
            result = (Response(query_result), status_code)
            result[0].headers['Content-type'] = 'application/json'
    
        elif status_code == NO_CONTENT:
            result = NO_CONTENT
    
    return result

@app.route('/volcano_activity/country/<name>', methods=['GET'])
def country_get(name):

    #Preset variables
    status_code, result = preset_result()
    args = {'name' : name}
    query_result, status_code = queries.country_get(args)
    result = (Response('<h1>{}</h1>'.format(query_result)), status_code)
    return result

@app.route('/volcano_activity/country', methods=['GET','POST'])
def country_post():

    #Preset variables
    status_code, result = preset_result()
    args = {'name' : None}
    if request.method == 'GET':
        query_result, status_code = queries.country_get(args)
        result = (Response('<h1>{}</h1>'.format(query_result)), status_code)
    elif request.method == 'POST':
        body = request.get_json(force=True)
        query_result, status_code = queries.country_post(body)
        result = (Response('<h1>{}</h1>'.format(query_result)), status_code)
    return result

###
if __name__ == '__main__':
    # Run this version of app.run to expose the server to the internal network
    app_host, app_debug = parse_from_cfg("server")
    app.run(host=app_host, debug=app_debug)
    
