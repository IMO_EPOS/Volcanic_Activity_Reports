"""
This script calls LibreOffice to convert a file (doc,odt,...) to pdf.

Created by Tim 2017-11-16:
Error handling is very primitive, consider improvements...
"""
import os
import sys
import subprocess


def convert_doc(source, ext, outdir):
    """ 
    INPUT: input_file, output_extension, output_dir
    OUTPUT: output_file_path
    EXAMPLE: convert_to('database/data','mydoc.odt','pdf')
        --> 'database/data/mydoc.pdf'
    """
    assert os.path.isfile(source), ('Could not find input file ' + source)
    args = [libreoffice_exec(), 
        '--headless', '--convert-to', ext, '--outdir', outdir, source]
    subprocess.check_call(args)
    p_root, p_ext = os.path.splitext(source)
    fbn = os.path.basename(p_root)
    outpath = (outdir + '/' + fbn + '.' + ext)
    
    if os.path.isfile(outpath):
        return outpath
    else:
        print 'Error: conversion with libreoffice failed!'
        sys.exit(1)
    
    return outpath


def libreoffice_exec():
    # TODO: Provide support for more platforms
    if sys.platform == 'darwin':
        return '/Applications/LibreOffice.app/Contents/MacOS/soffice'
    return 'libreoffice'


if __name__ == '__main__':
    print('Converted to ' + convert_doc(sys.argv[1], sys.argv[2], sys.argv[3]))
