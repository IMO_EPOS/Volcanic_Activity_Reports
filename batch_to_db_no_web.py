# -*- coding: utf-8 -*-
"""
From given dir, 
find all doc/odt volc.act.reports,
convert all to pdf in given out dir,
convert all to txt as well, (temporary files)
parse contents from txt into python objects,
remove txt files,
insert objects into PostgreSQL database.

INPUT: 
  indir  = source directory containing doc reports (no subdirs searched)
  outdir = target directory to receive pdf reports (no subdirs used)
  config = database config file (DEFAULT: set in parse_config.py)
OUTPUT:
  exit state number (0 = no errors)

Example usage:
python batch_to_db_no_web.py data/doc data/pdf
  
2017-11-22 tim
"""

# standard library imports:
import os
import re
import socket
import argparse
from collections import Counter

# SqlAlchemy imports
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# local app imports:
from parse_VARTF import parse_VolcActReportTxtFile # my report parser
from LO_doc2pdf import convert_doc # my doc conversion function
from utils import generate_file_md5 # function get MD5 hash for file
#from my_db_utils import safe_add # my function to INSERT or skip if CONFLICT
from VAR_utils import (
    insert_contact, 
    insert_report,
    insert_volcano_report,
    request_report_by_pdf_path,
    get_agency_id,
)
from parse_config import parse_from_cfg # my config parser


# DEFAULT for agency:
DEF_AGENCY = 'IMO'

def main():
    """Main function for lookup, conversion, parsing, db update.
    Go into given dir, 
    find all doc/odt volc.act.reports,
    convert all to pdf in given out dir,
    and also parse contents into some object,
    put that object into a PostgreSQL database.
    """
    # First check input
    parser = argparse.ArgumentParser(
        description='Lookup, convert, parse and update db of Volc.Act.Reports')
    parser.add_argument('indir', type=str, help='Source directory (doc)')
    parser.add_argument('outdir', type=str, help='Target directory (pdf)')
    parser.add_argument('config', type=str, help='DB config file', 
                        nargs='?', default=None)
    parser.add_argument('-v', '--verbose', help='increase output verbosity', 
                        action='store_true')
    args = parser.parse_args()
    
    # Defining verbose print depending on requested verbosity
    if args.verbose:
        def vprint(*args):
            # print all args in one line
            for arg in args:
                print arg, 
            print
    else:
        vprint = lambda *a: None  # do-nothing function
    
    # check dirs
    assert os.path.isdir(args.indir), 'Could not find {}'.format(args.indir)
    assert os.path.isdir(args.outdir), 'Could not find {}'.format(args.outdir)
    
    # get all doc, docx and odt Volc.Act.Reports
    ddir = args.indir
    fpat = ddir + '/EBE-.*\.(doc[x]?|odt)'
    flist = [os.path.join(ddir, f) for f in os.listdir(ddir)]
    inlist = [k for k in flist if re.match(fpat, k)]
    assert inlist, 'Source dir does not contain doc or odt files!'
    nf = len(inlist)
    vprint('Number of input files found: ', nf)
    
    # get DB connection info from config file
    if args.config:
        db_url, db_info = parse_from_cfg('database', args.config)
    else:
        db_url, db_info = parse_from_cfg('database')
    # setup DB connection and session
    engine = create_engine(db_url)
    session = sessionmaker()
    session.configure(bind=engine)
    my_session = session()
    
    # convert each report to pdf and parse its text
    inlist.sort()
    blist = [os.path.splitext(x)[0] for x in inlist]
    bcounts = Counter(blist)
    nskip1 = 0
    nskip2 = 0
    fqdn = socket.getfqdn()
    # for DEBUG   # DEBUG
    cnt = 0 # counter to break out if cnt > MAXCNT   # DEBUG
    MAXCNT = 999 # only do this many at the most   # DEBUG
    for infile in inlist:
        # metadata about input doc file
        abs_infile = fqdn + ':' +  os.path.abspath(infile)
        doc_size = os.path.getsize(infile)
        vprint('IN: ', infile, ', ', doc_size, ' B')
        
        p_root, ext = os.path.splitext(infile)
        if bcounts[p_root]>1:
            print '# Have multiple input files for ' + p_root
            if ext == '.odt':
                print '# Skipping odt file in favor of doc file.\n'
                nskip1 += 1
                continue
        
        # for DEBUG   # DEBUG
        cnt += 1   # DEBUG
        if cnt > MAXCNT:   # DEBUG
            break   # DEBUG
        
        # check DB: if output already exists, skip to next input
        fbn = os.path.basename(p_root)
        outpath = (args.outdir + '/' + fbn + '.pdf')
        abs_outpath = fqdn + ':' +  os.path.abspath(outpath)
        if request_report_by_pdf_path(my_session, abs_outpath):
            vprint('# Already have outfile ', abs_outpath)
            vprint('# Skipping this one.\n')
            nskip2 += 1
            continue
        
        # first convert to pdf and save in outdir
        opdf = convert_doc(infile, 'pdf', args.outdir)
        pdf_size = os.path.getsize(opdf)
        vprint('PDF: ', opdf, ', ', pdf_size, ' B')
        
        # then convert to txt, also save in outdir
        otxt = convert_doc(infile, 'txt', args.outdir)
        vprint('TXT: ', otxt)
        
        # get md5 checksum for original doc and new pdf
        md5_doc = generate_file_md5(infile)
        md5_pdf = generate_file_md5(opdf)
        
        # parse text file, get (meta)data dict 'tab'
        if args.verbose:
            tab = parse_VolcActReportTxtFile(otxt, '-v')
        else:
            tab = parse_VolcActReportTxtFile(otxt)
        
        # delete text file
        os.remove(otxt)
        
        #
        # postprocess and feed into DB
        #
        # check report author: can be two persons
        m = re.search(r'(.+)(?:\s+and|,|\s+&)\s+(.+)', tab['Author'])
        if m:
            id_contact1 = insert_contact(
                my_session, {'name': m.group(1), 'agency': DEF_AGENCY})
            id_contact2 = insert_contact(
                my_session, {'name': m.group(2), 'agency': DEF_AGENCY})
        else:
            id_contact1 = insert_contact(
                my_session, {'name': tab['Author'], 'agency': DEF_AGENCY})
            id_contact2 = None
        
        # Report
        # What about the missing values? Can we get something here...?
        id_agency = get_agency_id(my_session, DEF_AGENCY)
        nvolc = len(tab['Volcanoes'])
        rep = dict(
            title=None,
            abstract=None,
            file_pdf=abs_outpath,
            md5_hash_pdf=md5_pdf,
            filesize_pdf=pdf_size,
            url_pdf=None,
            file_doc=abs_infile,
            md5_hash_doc=md5_doc,
            filesize_doc=doc_size,
            report_type=None,
            type_of_information=None,
            date_publication=tab['Date_pub'],
            date_valid_from=tab['ValidFrom'],
            date_valid_to=tab['ValidTo'],
            id_agency=id_agency,
            id_contact=id_contact1,
            id_contact_2=id_contact2,
            number_of_volcanoes=nvolc,
            additional_notes=tab['Notes']
        )
        eid, msg, id_report = insert_report(my_session, rep)
        vprint('Report ID: ', id_report)
        # go to next report if this one was not inserted successfully
        if eid != 'success':
            continue
        
        # Volcano specifics:
        vprint('Have got ',nvolc,' volcanoes in this report.')
        for volc in tab['Volcanoes']:
            # [vna, vla, vlo, vid, vcc, vcm]
            vdict = dict(
                id_report=id_report,
                id_volcano=volc[0],
                color_code=volc[4],
                status_text=volc[5],
                additional_notes=None
            )
            eid, msg, vid = insert_volcano_report(my_session, vdict)
            vprint('Volcano:',volc[0],', ID:',vid,', INSERT:',eid)
        
    
    # some final log info
    print 'Files skipped: {} (conflicting odt/doc, take doc)'.format(nskip1)
    print 'Files skipped: {} (pdf already existed)'.format(nskip2)
    # this is what's up
    print 'All done.'
    

if __name__ == '__main__':
    main()
