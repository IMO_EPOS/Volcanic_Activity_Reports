"""
Parse a text version of Volcanic Activity Reports files,
save specific info to some data structure.
This version searches for keywords to extract info. No specific keyword order
is expected, but when found, each text block must follow a simple fixed pattern.

2017-11-17 tim
"""

# standard library imports:
import argparse
import re
import os
import socket

# This is the main function
def parse_VolcActReportTxtFile(*arglist):
    """Take a txt file, parse it and (hopefully) return its contents as obj.
    """
    # Get the file
    parser = argparse.ArgumentParser(
        description='Parse Volc.Act.Report txt file')
    parser.add_argument('file', type=str)
    parser.add_argument('-v', '--verbose', help='increase output verbosity', 
                        action='store_true')
    args = parser.parse_args(arglist)
    
    # Defining verbose print depending on requested verbosity
    if args.verbose:
        def vprint(*args):
            # print all args in one line
            for arg in args:
                print arg, 
            print
    else:
        vprint = lambda *a: None  # do-nothing function
    
    # Read it and parse text
    with open(args.file, 'rb') as f:
        
        txt = f.read()
        # cut BOM if present (yes, LibreOffice puts it into txt files)
        if re.match(r'\xef\xbb\xbf', txt):
            txt = txt[3:]
        
        list1 = re.split(r'\n{2,}', txt) # most info is separated by double NL
        
        # initialize dict:
        table = dict.fromkeys([ 'URI',
                                'Date_pub',
                                'ValidFrom',  
                                'ValidTo', 
                                'Author', 
                                'Volcanoes', 
                                'Notes', 
                                'FileBaseName'])
        table['URI'] = socket.getfqdn() + ':' +  os.path.abspath(args.file)
        table['FileBaseName'] = os.path.basename(args.file)
        
        # this pattern is required because at least one report had
        # a monthname written instead of the usual number:
        dpat = re.compile(r'\d{1,2}.\d{1,2}.\d{4}|\d{1,2}.\w{3,8}.\d{4}')
        for A in list1:
            # Get date and time
            if re.match(r'Date and time', A):
                table['Date_pub'] = dpat.search(A).group(0)
                continue
            # Get time of validity
            if re.match(r'Week of validity', A):
                m = dpat.findall(A)
                table['ValidFrom'] = m[0]
                table['ValidTo'] = m[1]
                continue
            # Get report author (some have brackets, remove them)
            if re.match(r'Reported by', A):
                table['Author'] = re.search(
                    r'(Reported by:)\s*(.*)\s*', A).group(2).strip(' \t[]')
                continue
            
        
        # Separate text block matching can get us only this far.
        # Get the additional notes:
        p = r'(Volcano name\n)(.*?)(\n\* Volcano appears quiet)'
        m = re.search(p, txt, re.DOTALL)
        if not m:
            p = r'(Volcano name\n)(.*?)(\n+Additional notes)'
            m = re.search(p, txt, re.DOTALL)
        
        if m:
            A = m.group(2).strip()
            # Get all volcano info from table
            if re.match(r'Latitude', A):
                table['Volcanoes'] = [] # start out with empty list
                # try to parse the table
                volc_tbl = re.split(r'\xc2\xb0', A) # split at degree symbol
                # first entry: header + first volcano
                v1 = re.split(r'\n', volc_tbl[0]) 
                vna = v1[6].strip() # volcano name
                vla = v1[7].strip() # volcano latitude
                
                # go through the rest of the volcano table
                getout = False
                nvt = len(volc_tbl)
                cnt = 1
                for vv in volc_tbl[1:]:
                    cnt += 1
                    v1 = re.split(r'\n', vv)
                    nl = len(v1)
                    if nl < 3: # have a longitude line
                        vlo = v1[1].strip() # volcano longitude
                        continue
                    
                    # should still have some info left for current volcano
                    vid = v1[1].strip() # volcano id number (Smithsonian)
                    vcc = v1[2].strip() # volcano color code
                    # volcano comments can be multiple lines, but careful with
                    # last one:
                    if cnt < nvt:
                        vcm = '\n'.join(v1[3:-2])
                    else:
                        vcm = '\n'.join(v1[3:])
                        getout = True
                    
                    vcm = re.sub('\nL\xc3\xbdsuskar\xc3\xb0/$','',vcm)
                    # update table
                    table['Volcanoes'].append(
                        [vna, vla, vlo, vid, vcc, vcm]) 
                    
                    # if current block is at table end
                    if getout:
                        break
                    # otherwise can get next volcano info
                    vna = v1[-2].strip() # volcano name
                    vla = v1[-1].strip() # volcano latitude
            else:
                print 'WARNING: Volcano table is unusual!'
        else:
            print '# Could not find any volcano table!'
            print '# Finished this report without it...'
            print '# file: {}'.format(table['URI'])
        
        
        # Separate text block matching can get us only this far.
        # Get the additional notes:
        p = r'(Additional notes:\n)(.*?)(\nThe report is issued)'
        m = re.search(p, txt, re.DOTALL)
        if m:
            table['Notes'] = m.group(2).strip()
        else:
            print '# Could not find any additional notes!'
            print '# Finished this report without them...'
        
        # That's all text parsing for this report, closing file...
    
    # Check whether everything is there, except Notes
    assert table['URI'] ,  'Could not store original file location!'
    assert table['FileBaseName'] , 'Could not store file basename!'
    assert table['Author'] ,  'Could not store author!'
    assert table['Date_pub'] , 'Could not store publication date!'
    assert table['ValidFrom'] , 'Could not store validity start date!'
    assert table['ValidTo'] , 'Could not store validity end date!'
    assert table['Volcanoes'] , 'Could not store the volcano table!'
    
    # Some debug/verbosity prints:
    vprint('File: \n', table['URI'])
    vprint('Input File: ', table['FileBaseName'])
    vprint('Author: ', table['Author'])
    vprint('Date published: ', table['Date_pub'])
    vprint('Valid from ', table['ValidFrom'], ' to ',  table['ValidTo'])
    vprint('')
    vprint(table['Volcanoes'][0])
    vprint(table['Volcanoes'][1])
    vprint(table['Volcanoes'][-1])
    # show final UTF char:
    vprint(table['Volcanoes'][-1][0].decode('string_escape')) 
    vprint('')
    vprint('Notes:\n', table['Notes'])
    vprint('All done: Text parsing')
    
    return table

# def main() over.

if __name__ == '__main__':
    parse_VolcActReportTxtFile()
