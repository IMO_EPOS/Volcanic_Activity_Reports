#  Volcanic Activity Reports

This folder contains functions for the Volcanic Activity Reports (VolcActRep) product, which is DDSS-31 in WP11 of EPOS.

* **Author:** Tim Sonnemann (tim@vedur.is)
* **Contributors and role:**
    * Tim Sonnemann: Current dev (IMO)
    * Sara Barsotti: VO rep for this service (IMO)
* **Date created:** 2017-11-14
* **Date last update:** 2018-01-11

---
## Directory Overview

* `database`:    resources and info for the actual (meta)data DB
* `data`:      some example reports (doc/odt)
* `templates`:   contains html template for main web page
* `swagger`:     contains YAML file and hints for web API documentation with Swagger
* `postman`:    contains JSON file for web API testing with Postman

---
## Requirements

### Main software

 *  `postgresql`
 *  `python` (version 2.7, not yet ported to 3.x)
 *  `libreoffice` (if reports are doc files and should be parsed)

### Python packages

 *  `flask`
 *  `requests`
 *  `sqlalchemy`
 *  `sqlalchemy_utils` (recommended)

---
## Create PostgreSQL Database

A new database shall be created for this task.

### Define its structure

This project has a tentative metadata definition given in an `database/Metadata Catalogue_REPORT_update 2017_05_16.xls`. Either one table could contain all info for all entries, or we could split it into multiple tables, e.g. one for Contact, Agency, Country, Volcano, Report, ReportData. The latter option is chosen here.

### Create the new database

#### Using the Command Line Interface

This uses the simple structure converted from the `xls` file.

Create a new database:
```
# createdb -U <user> [-h <host>] <db_name>
createdb -U epos -h 127.0.0.1 volcanic_activity_reports
```
Import the DB schema based on SQL script:
```
psql -U epos -h 127.0.0.1 -d volcanic_activity_reports < database/volcanic_activity_reports.pgdump.sql
```
TO DO: store schema file there.

#### Using a Python script

Copy the configuration file `db_config.cfg.default` to `db_config.cfg` and edit the user name, password and database name to your local preferences. Then a new database can be created with
```
python create_db.py
```
To define the database structure according to `volc_act_rep_datamodel.py`, do
```
python define_db.py
```

#### Using GUI software

Create new DB in some GUI (`DBeaver` *FOSS*, `SQuirreL SQL Client` *FOSS*,
`DbSchema` *commercial*, or [others](https://en.wikipedia.org/wiki/Comparison_of_database_tools))

E.g., `DBeaver` has an SQL editor (code completion, syntax highlighting), so
after creating a new DB, open the editor and copy-paste & execute the prepared commands from file `volcanic_activity_reports.pgdump.sql`.

Check if all is well, edit as needed.


### Delete a database

In case the development database shall be erased and created from scratch, the deletion can be done by CLI:
```
#dropdb [-U <username> -h <hostname> -i -e] <db_name>
dropdb -U epos -i -e test_volactrep
```

### Delete and recreate database

To drop and recreate the database in one step, modify the bash script `destroy_recreate.sh.default` and remove the `.default` ending, then run it:
```
./destroy_recreate.sh
```
This also populates the volcano table.

---
## Populate DB Table with data

DB input is metadata, which must be generated from actual data.
In this case, report doc files need to be parsed.

Therefore, Processing steps are:

* Get (new) doc/odt file and convert to pdf
* Parse doc/pdf "intelligently",
* Postprocess parsed info into metadata format,
* Update DB with metadata.


### Convert doc/odt to pdf

Can be done easily using Libreoffice on any system:
```
libreoffice --headless --convert-to pdf --outdir <OUTDIR> <SOURCE>
```
The source can be doc, docx, odt (anything Libreoffice can use), and the process is quite fast for small files (<1 sec).

This can be done from a Python script by using the `subprocess` package for system calls. A useful example can be found [here](https://michalzalecki.com/converting-docx-to-pdf-using-python/#running-libreoffice-as-a-subprocess).

### Parse pdf file

Bad idea, parsing PDF files is complete madness. The returned objects of some available parsers contain no particular structure and do not even seem to have the expected text strings. Tried modules: `PyPDF2` and `PDFMiner`.

Possibly still the best for that task would be `Xpdftools` (*FOSS*), which can
convert PDFs to structured text files through a smart graphical object handling algorithm.

### Parse txt file

Instead, we will convert all doc/odt files also to `.txt` files using LibreOffice, which results in a simple and clearly structured output. Parsing that isn't so hard.
I have written a Python 2 function for that: `parse_VARTF.py`,

### Enter data into database

Let's enter the first basic information into the database, like countries, agencies, volcanoes and maybe a few contacts. A list of volcanoes was exported as `csv` file from another database, which will be parsed and inserted into the new database. A Python script will do that, but it's mostly hardwired and only supposed to be a first step.
```
python write_first_data_to_db.py database/volcano_table_nocomm_v2.csv -v
```
The idea is to now go ahead and parse all reports that would be linked only to already existing entries of countries, agencies, volcanoes etc. That way, we need not worry too much about going back and updating all foreign keys to previously unknown entities.

For that, the `txt` parser from above is used from a script `batch_to_db_no_web.py` which searches all `doc/odt` files in a given directory, converts to `pdf`, reads metadata, connects to the database (using the config file), and then inserts the (meta)data in a controlled fashion.
```
python batch_to_db_no_web.py data/doc data/pdf
```

### Automatically update the database

All report doc files seem to be in:
```
nas.vedur.is:/export/gos/verk/VOLCANO_WEEKLY_REPORT
```

---
## Web Server

To make the database accessible online, start the Flask web server script:
```
python volc_act_rep_webserver.py
```
A file `queries.log` will be produced and gradually appended to for all server interactions.

### Testing the web server

For a few simple tests, install [Postman](https://www.getpostman.com/) and import the JSON file in the `postman` directory of this project. Various predefined queries can then be made on the push of a button.

### Online web API documentation

With [Swagger](https://swagger.io/), the API documentation file `swagger/volcanic_activity_reports.yaml` can be used to create a convenient interface for both documentation and interactive testing of functions. 

The YAML file could simply be pasted into the [online swagger editor](https://editor.swagger.io//#/), that should work. The Flask server has [enabled CORS](https://enable-cors.org/server.html) by using `flask_cors` for this.
















