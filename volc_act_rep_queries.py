'''

This script defines possible web service endpoints and associated functions
for web queries to the Volcanic Activity Report web services.

2017-11-13 tim: created

'''

## General imports
import sys
import json
import logging
import logging.handlers
import decimal
from collections import OrderedDict
from contextlib import closing
from datetime import datetime
from httplib import (
    NO_CONTENT,
    OK,
    NOT_FOUND,
    CONFLICT,
    BAD_REQUEST,
    INTERNAL_SERVER_ERROR,
    CREATED,
)

## SqlAlchemy imports
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, aliased

## My local imports
from volc_act_rep_datamodel import (
    Agency, Contact, Country, Report, Volcano, VolcanoReport,
) # my database classes
from my_db_utils import row2dict
from VAR_utils import (
    insert_country, is_valid_date, check_date,
)


## Support classes ##
class JSONCustomEncoder(json.JSONEncoder):
    """ Custom JSON encoder to handle datetime objects
    """
    def default(self, obj):

        if isinstance(obj, datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')

        if isinstance(obj, decimal.Decimal):
            return float(obj)

        return json.JSONEncoder.default(self, obj)

## Main class
class Queries(object):
    """
    This object has a datbase connection and provides a list of possible
    actions on that database.
    """
    
    ### Init ###
    def __init__(self, db_url):
        """
        Setup Db connection and start logger to file 'queries.log'.
        """

        ## -------------------------------------------------- ##
        ## 1) Set up database connection and bind to session  ##
        ## -------------------------------------------------- ##

        engine = create_engine(db_url)
        self.session = sessionmaker(bind=engine)


        ## ----------------- ##
        ## 2) Define logger  ##
        ## ----------------- ##

        # create logger with current application
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

        # create file handler which logs even debug messages
        f_handler = logging.handlers.RotatingFileHandler(
            'queries.log', maxBytes=20, backupCount=0)
        f_handler.setLevel(logging.DEBUG)

        # create console handler with a higher log level
        c_handler = logging.StreamHandler()
        c_handler.setLevel(logging.ERROR)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s',
                                       datefmt='%m/%d/%y %H:%M')
        # create formatter and add it to the handlers
        f_handler.setFormatter(formatter)
        c_handler.setFormatter(formatter)

        # add the handlers to the logger
        self.logger.addHandler(f_handler)
        self.logger.addHandler(c_handler)

        self.logger.info("Logger has been defined")
        self.logger.info("Queries object instantiated")

    ### Services ###
    def my_template_fct(self, args):
        """
        DOCSTRING!
        """
        myname = sys._getframe().f_code.co_name
        self.logger.info(myname + '() was called')
        ## Preset result
        result = None
        query_list = None
        status_code = NO_CONTENT
        response_list = []

        
        with closing(self.session()) as session:

            self.logger.info('Just before the query...')
            query_list = session.query(Report, Contact)\
                    .join(Contact, Report.contact==Contact.name)\
                    .all()
            
            if query_list:
                # turn query_list into dict, then restructure to JSON format
                md = {}
                # Go through all output table collections
                for qobj in query_list:
                    # Convert each table's row to dict
                    for qrow in qobj:
                        md[qrow.__tablename__] = row2dict(qrow)
            
                    response = OrderedDict([
                        ('name', md['volcano']['name']),
                        ('si_name', md['volcano']['si_name']),
                        ('country', md['country']['name']),
                    ])
                    response_list.append(response)
                
                result = json.dumps(response_list, cls=JSONCustomEncoder)
                status_code = OK
            else:
                # No result - no content
                status_code = NO_CONTENT
                result = None
        
        return result, status_code
    
    
    def report_list_get(self, args):
        """
        Return list of all available reports (no args)
        Return a single report by id (args.report_id)
        Return list of reports by date range (args.date_from,date_to)
        """
        myname = sys._getframe().f_code.co_name
        self.logger.info(myname + '() was called')
        ## Preset result
        result = None
        query_list = None
        status_code = NO_CONTENT
        response_list = []
        var = {'id_report': None,
               'date_from': None,
               'date_to': None,
               'agency': None}
        vn = {'id': 'id_report', 
              'fr': 'date_from', 
              'to': 'date_to', 
              'ag': 'agency'}
        with closing(self.session()) as session:
            # Check requested specifics of response:
            if args:
                k1 = 'id_report'
                if k1 in args:
                    if args[k1].isdigit():
                        self.logger.info('{} = {}'.format(k1, args[k1]))
                        var[k1] = int(args[k1])
                    else:
                        self.logger.info(
                            'Invalid format: {} = {}'.format(k1, args[k1]))
                k1 = 'date_from'
                if k1 in args:
                    if is_valid_date(args[k1]):
                        self.logger.info('{} = {}'.format(k1, args[k1]))
                        var[k1] = check_date(args[k1])
                    else:
                        self.logger.info(
                            'Invalid format: {} = {}'.format(k1, args[k1]))
                k1 = 'date_to'
                if k1 in args:
                    if is_valid_date(args[k1]):
                        self.logger.info('{} = {}'.format(k1, args[k1]))
                        var[k1] = check_date(args[k1])
                    else:
                        self.logger.info(
                            'Invalid format: {} = {}'.format(k1, args[k1]))
                k1 = 'agency'
                if k1 in args:
                    self.logger.info('{} = {}'.format(k1, args[k1]))
                    var[k1] = args[k1]
            
            # TODO: can have 2 contacts per report, get both somehow:
            contact1 = aliased(Contact, name='contact1')
            #contact2 = aliased(Contact, name='contact2')
            
            # different filters:
            if var['id_report']:
                query_list = session.query(
                    Report, Contact, Agency, Country)\
                        .join(Contact, Report.id_contact==Contact.id)\
                        .join(Agency, Report.id_agency==Agency.id)\
                        .join(Country, Agency.id_country==Country.id)\
                        .filter(Report.id == var['id_report'])\
                        .all()
            elif var[vn['fr']] or var[vn['to']] or var[vn['ag']]:
                filters = ()
                if var[vn['fr']]:
                    filters += (Report.date_valid_from >= var[vn['fr']] ,)
                if var[vn['to']]:
                    filters += (Report.date_valid_to <= var[vn['to']] ,)
                if var[vn['ag']]:
                    filters += (Agency.abbreviation == var[vn['ag']] ,)
                query_list = session.query(
                    Report, contact1, Agency, Country)\
                        .join(contact1, Report.id_contact==contact1.id)\
                        .join(Agency, Report.id_agency==Agency.id)\
                        .join(Country, Agency.id_country==Country.id)\
                        .filter(*filters)\
                        .order_by(Report.date_valid_from)\
                        .all()
            else:
                query_list = session.query(
                    Report, Contact, Agency, Country)\
                        .join(Contact, Report.id_contact==Contact.id)\
                        .join(Agency, Report.id_agency==Agency.id)\
                        .join(Country, Agency.id_country==Country.id)\
                        .order_by(Report.date_valid_from)\
                        .all()
            
            if query_list:
                # turn query_list into dict, then restructure to JSON format
                md = {}
                # Go through all output table collections
                for qobj in query_list:
                    # Convert each table's row to dict
                    for qrow in qobj:
                        md[qrow.__tablename__] = row2dict(qrow)
            
                    response = OrderedDict([
                        ('date_published', md['report']['date_publication']
                                            .strftime('%Y-%m-%d')),
                        ('valid_from', md['report']['date_valid_from']
                                            .strftime('%Y-%m-%d')),
                        ('valid_to', md['report']['date_valid_to']
                                            .strftime('%Y-%m-%d')),
                        ('agency', md['agency']['abbreviation']),
                        ('country', md['country']['name']),
                        ('contact', 
                            OrderedDict([
                                ('name', md['contact']['name']),
                                ('agency', md['agency']['abbreviation'])
                            ])),
                        ('n_volc', md['report']['number_of_volcanoes']),
                        ('URI', md['report']['file_pdf']),
                        ('filesize_B', md['report']['filesize_pdf']),
                    ])
                    response_list.append(response)
                
                result = json.dumps(response_list, cls=JSONCustomEncoder)
                status_code = OK
            else:
                # No result - no content
                status_code = NO_CONTENT
                result = None
        
        return result, status_code
    
    
    def volcano_list_get(self, args):
        """
        Return a list of URIs of all available volcanoes
        """
        myname = sys._getframe().f_code.co_name
        self.logger.info(myname + '() was called')
        ## Preset result
        result = None
        query_list = None
        status_code = NO_CONTENT
        response_list = []

        with closing(self.session()) as session:

            self.logger.info('Just before the query...')
            query_list = session.query(Volcano, Country)\
                    .join(Country)\
                    .all()
            
            if query_list:
                # turn query_list into dict, then restructure to JSON format
                md = {}
                # Go through all output table collections
                for qobj in query_list:
                    # Convert each table's row to dict
                    for qrow in qobj:
                        md[qrow.__tablename__] = row2dict(qrow)
            
                    response = OrderedDict([
                        ('name', md['volcano']['name']),
                        ('si_name', md['volcano']['si_name']),
                        ('alt_name', md['volcano']['alt_name']),
                        ('icao_code', md['volcano']['icao_code']),
                        ('country_code', md['country']['iso_code']),
                        ('country', md['country']['name']),
                    ])
                    response_list.append(response)
                
                result = json.dumps(response_list, cls=JSONCustomEncoder)
                status_code = OK
            else:
                # No result - no content
                status_code = NO_CONTENT
                result = None
        
        return result, status_code
    
    def country_get(self, args):
        
        status_code = OK
        result = 'Service has not been implemented'
        return result, status_code
    
    
    def country_post(self, args):
        """
        Store a new country in database.
        """
        myname = sys._getframe().f_code.co_name
        self.logger.info(myname + '() was called')
        result = None
        status_code = NO_CONTENT
        country_name = None
        
        if args['name']:
            country_name = args['name']
                
        with closing(self.session()) as session:
            eid, msg, xid, country = insert_country(session, country_name)
            result = '{}, {}, id:{}, country: {}'.format(
                    eid, msg, xid, country)
            self.logger.info('{}(): {}'.format(myname, result))
            status_code = OK
        
        return result, status_code
