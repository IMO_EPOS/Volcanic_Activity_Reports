"""
Here should be at least one utility function for database handling
using SQLAlchemy.

This is meant to be generally applicable for all (PostgreSQL) projects with
regards to database handling.

2017-11-22 tim: created
"""
from sqlalchemy.exc import IntegrityError
#
def safe_add(my_session, my_table):
    """Add new entry to DB but do nothing if uniqueness conflict.
    Returns None is successful, else returns error message string.
    """
    #id = None # this does not work well in Python
    xid = -1 # unfortunate but must choose something not-None as empty default
    try:
        my_session.add(my_table)
        my_session.commit()
        xid = my_table.id # won't get ID if UNIQUE VIOLATION
    except IntegrityError as err:
        my_session.rollback()
        msg = err.args[0]
        if 'unique constraint' in msg:
            print 'Warning: Trying to add duplicate, do nothing, details:'
            print msg
            eid = 'unique'
        elif 'foreign key constraint' in msg:
            print 'Warning: Foreign key constraint error, do nothing, details:'
            print msg
            eid = 'foreign'
        else:
            print 'Warning: Unknown IntegrityError, do nothing, details:'
            print msg
            eid = 'unknown'
        return eid, msg, xid
    except:
        print ('# UNEXPECTED ERROR!')
        raise
    else:
        return 'success', 'successful execution', xid

#
def row2dict(row):
    """
    Convert sqlalchemy query output object into dict.
    This avoids having to deal with 'column.__dict__', which includes an extra
    field '_sa_instance_state' and may or may not get more in future releases.
    """
    d = {}
    for column in row.__table__.columns:
        d[column.name] = getattr(row, column.name)
    return d