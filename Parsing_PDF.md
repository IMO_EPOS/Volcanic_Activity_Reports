*2017-11-24 Tim Sonnemann*

# Parse PDF

## Xpdf Tools

Free open source products (GPL) by [Glyph & Cog](http://www.xpdfreader.com/download.html),
which include `pdftohtml` and `pdftotext`.
They can be used on any platform with a C++ compiler
(Linux, Mac, Windows) as terminal commands.

### Command: `pdftohtml`

Using `pdftohtml`, a pdf file can be converted to both html and xml with a 
relatively good approximation of the original structure.

#### To HTML:
```
pdftohtml -s -i EBE-032-4_Volcano_weekly_status_report_20171115.pdf
```
Options `-s` for keeping it a single output file (instead of pages),
and `-i` for ignoring images.

__Important:__ Paragraphs in the *Additional Notes* will be marked by
`<p>...paragraph text...</p>`. There are lots of html-specific tags
and also "non-breakable space symbols" (`&#160;`) that would all have to
be parsed out correctly.

#### To XML:
```
pdftohtml -s -i -xml EBE-032-4_Volcano_weekly_status_report_20171115.pdf
```
Options as above, with `-xml` added to get XML output file.

__Important:__ Paragraphs are not marked by anything, each line is treated
separately. There are many tags but slightly less than in the html output.

### Command: `pdftotext`

A simple text file can be obtained by using `pdftotext` on the pdf. Two
kinds of non-HTML text are available: one without any special format with
text as it was written to the pdf, and another one that resembles the
physical structure of the pdf.

#### Simple text

```
pdftotext -nopgbrk EBE-032-4_Volcano_weekly_status_report_20171115.pdf
```
This produces the simplest text version. Option `-nopgbrk` makes it
ignore page breaks to have one simple unbroken document.

__Important:__ The table is unrolled with one cell per line with an extra
empty line in between. The paragraphs in the *Additional Notes* are not
preserved, such that it becomes one text block without empty lines.

#### Text with layout

```
pdftotext -nopgbrk -layout EBE-032-4_Volcano_weekly_status_report_20171115.pdf
```
This produces the structured text version through option `-layout`.

__Important:__ The table has one line per row, which is quite useful.
The paragraphs in the *Additional Notes* are preserved through empty lines.

## Conclusion

The HTML output and layout text are useful, XML and simple text output 
are not adequate.

The layout text is most likely easier to parse than the HTML output.

The layout-preserved text output should be used for this task.