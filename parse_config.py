# -*- coding: utf-8 -*-
"""
This function takes a config file and returns either a database connection 
string as 'type/user:password/host:port/dbname' and a matching dict, OR
it returns web server configuration values, depending on the input option.

INPUT: configtype (DEFAULT: 'database', only other choice: 'server')
       configfile (DEFAULT: 'db_config.cfg')
       
OUTPUT: 
  # for database:
        db_url = string usable as url connection info
        db_info = dict containing the strings for 
                    dbtype, username, password, hostname, port, dbname
  # for server:
        app_run = debug switch, True or False, for Flask run()
        app_host = hostname for Flask run() 

TODO: Change the hardwired format, as url does not always really need 
      all the arguments. An engine.url object can have some None...

Created on Wed Nov 29 11:41:58 2017
@author: tim
"""

# standard library imports:
import os
import argparse
from ConfigParser import SafeConfigParser

# This is the main function
def parse_from_cfg(*arglist):
    # Check input
    parser = argparse.ArgumentParser(
        description='Create database by config file (DEFAULT: db_config.cfg)')
    parser.add_argument('configtype',
                        type=str,
                        nargs='?',
                        choices=['database','server'],
                        default='database')
    parser.add_argument('configfile',
                        type=str,
                        nargs='?',
                        default='db_config.cfg')
    args = parser.parse_args(arglist)
    fcfg = args.configfile
    ctyp = args.configtype
    
    # Check if config file exists
    assert os.path.isfile(fcfg), ('Could not find config file ' + fcfg)
    
     # including defaults: postgresql port and app_host
    config = SafeConfigParser({'port':5432,'app_host':None})
    config.read(fcfg)
    
    # Pull database settings info from config file, store in dict
    if ctyp == 'database':
        db_info = { 'db_type':config.get("Database", "db_type"), 
                    'username':config.get("Database", "username"), 
                    'password':config.get("Database", "password"),
                    'hostname':config.get("Database", "hostname"), 
                    'port':config.get("Database", "port"), 
                    'db_name':config.get("Database", "db_name")
                  }
        db_url = ('{0}://{1}:{2}@{3}:{4}/{5}'.format(
                    db_info['db_type'],
                    db_info['username'],
                    db_info['password'],
                    db_info['hostname'],
                    db_info['port'],
                    db_info['db_name']))
        return db_url, db_info
    
    # Pull server settings info from config file, return two parameters
    if ctyp == 'server':
        app_debug = config.get("Server", "app_debug")
        app_host  = config.get("Server", "app_host")
        return app_host, app_debug
    
    # Wrong input is detected by argparser above.

#
if __name__ == '__main__':
    parse_from_cfg()
