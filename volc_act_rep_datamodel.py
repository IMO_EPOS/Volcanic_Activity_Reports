"""Definitions of database tables.

This file gives classes corresponding to PostgreSQL database tables with all 
their entries using sqlalchemy.

2017-11-14 tim: created
2017-11-21 tim: first finished version
2017-11-22 tim: added unique keys to some fields
2017-11-28 tim: some column changes, new: 'Volcano.alt_name'
"""

# SqlAlchemy imports
from sqlalchemy import (
    BigInteger,
    Column, 
    Date,
    Float,  
    ForeignKey, 
    Integer, 
    String, 
    Text, 
)
from sqlalchemy.ext.declarative import declarative_base

# Declarations
Base = declarative_base()
#
## Data Models
#
class Contact(Base):
    """Contact person, author of report"""

    __tablename__ = 'contact'
    id = Column(Integer, primary_key=True)
    name = Column(String(255), unique=True)
    title = Column(String(255))
    email = Column(String(255))
    phone = Column(String(255))
    gsm = Column(String(255))
    comment = Column(Text)
    role = Column(String(255))
    id_agency = Column(Integer, ForeignKey('agency.id'))

class Agency(Base):
    """Issuing agency of report"""

    __tablename__ = 'agency'
    id = Column(Integer, primary_key=True)
    name = Column(String(128), unique=True)
    abbreviation = Column(String(32), unique=True)
    address = Column(Text)
    www = Column(String(255))
    infos = Column(Text)
    id_country = Column(Integer, ForeignKey('country.id'))

class Country(Base):
    """Country reference"""

    __tablename__ = 'country'
    id = Column(Integer, primary_key=True)
    name = Column(String(255), unique=True)
    iso_code = Column(String(8), unique=True)

class Report(Base):
    """Each report should have this info"""
    
    __tablename__ = 'report'
    id = Column(Integer, primary_key=True)
    title = Column(Text)
    abstract = Column(Text)
    file_pdf = Column(Text, unique=True)
    md5_hash_pdf = Column(String(32))
    filesize_pdf = Column(BigInteger)  # filesize in bytes
    url_pdf = Column(Text, unique=True)
    file_doc = Column(Text, unique=True)
    md5_hash_doc = Column(String(32))
    filesize_doc = Column(BigInteger)  # filesize in bytes
    report_type = Column(Text)
    type_of_information = Column(Text)
    date_publication = Column(Date)
    date_valid_from = Column(Date)
    date_valid_to = Column(Date)
    id_agency = Column(Integer, ForeignKey('agency.id'))
    id_contact = Column(Integer, ForeignKey('contact.id'))
    id_contact_2 = Column(Integer, ForeignKey('contact.id')) # if two given
    number_of_volcanoes = Column(Integer)
    additional_notes = Column(Text)
    

class Volcano(Base):
    """List of known volcanoes with metadata"""
    
    __tablename__ = 'volcano'
    id = Column(Integer, primary_key=True)
    name = Column(String(128), unique=True)
    si_name = Column(String(128))
    alt_name = Column(String(128)) # allow alternative name for better search
    lat_dd = Column(Float) # int in old DB, this has got be wrong...
    lon_dd = Column(Float) # --> setting this as float type here.  
    lat_dms = Column(String(32))
    lon_dms = Column(String(32))
    lat_nggmm = Column(String(32))
    lon_ngggmm = Column(String(32))
    height = Column(Integer)
    icao_code = Column(String(32), unique=True) # should be int??? 
    area = Column(String(128))
    id_country = Column(Integer, ForeignKey('country.id'))

class VolcanoReport(Base):
    """Actual report text for each volcano from each report"""

    __tablename__ = 'volcano_reports'
    id = Column(Integer, primary_key=True)
    id_volcano = Column(Integer, ForeignKey('volcano.id'))
    id_report = Column(Integer, ForeignKey('report.id'))
    color_code = Column(String(32))
    status_text = Column(Text)
    additional_notes = Column(Text)


def get_the_base():
    """This function should be under all db models,
    to be called whenever a new db needs to be assigned its structure.
    The define_db.py script should do: Base.metadata.create_all(engine)
    
    TODO: There is probably a better way of doing this, so read the docs.
    """
    return Base










