"""
This script creates a new database using the type/user/password/host/dbname
given in a config file.
If no config file name provided, use default db_config.cfg.

2017-11-21 tim: created, works fine
"""

# SqlAlchemy imports
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database

# Use the local config parser
from parse_config import parse_from_cfg

# This is the main function
def create_db_from_cfg():
    # get db connection info from config file
    db_url, db_info = parse_from_cfg('database')
    # setup db connection
    engine = create_engine(db_url)
    # create db if not there yet
    if database_exists(engine.url):
        print 'Database {} exists already, do nothing.'.format(
            db_info['db_name'])
    else:
        print 'Creating new database: {}'.format(db_info['db_name'])
        create_database(engine.url)
    
    print 'Done.'

# def main() over.
if __name__ == '__main__':
    create_db_from_cfg()
