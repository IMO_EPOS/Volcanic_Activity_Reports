"""
Take an empty db and a datamodel, and create the empty tables

"""

# SqlAlchemy imports
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists

# Use the local config parser and datamodel
from parse_config import parse_from_cfg
from volc_act_rep_datamodel import (
    get_the_base, 
    #Contact, Agency, Country, Report, Volcano, VolcanoReports, 
)

def setup_db_tables():
    # get db connection info from config file
    db_url, db_info = parse_from_cfg('database')
    # setup db connection
    engine = create_engine(db_url)
    if not database_exists(engine.url):
        print (
            '# The database does not exist yet. \n'
            '# It should be created automatically by creating tables. \n'
            "# (If not, use function 'create_db.py' for that)")
    get_the_base().metadata.create_all(engine)
    print 'Have assigned datamodel to database.'


# def main() over.
if __name__ == '__main__':
    setup_db_tables()
